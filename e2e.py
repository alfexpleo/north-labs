import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver import ChromeOptions

class Test_checkout():
  def setup_method(self, method):
    #configure chrome in headless to work in containers
    options = ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--disable-extensions')
    options.add_argument("--no-sandbox")
    self.driver = webdriver.Remote('http://localhost:4444/wd/hub', options.to_capabilities())

    self.driver.get("http://localhost:8000/")
    self.driver.set_window_size(1116, 864)
    # Assert to check if there is a SHOP button to enter into the shop
    elements = self.driver.find_elements(By.CSS_SELECTOR, ".site-menu > li:nth-child(2) > a")
    assert len(elements) > 0
    # Assert to check if there is a shopping cart button
    elements = self.driver.find_elements(By.CSS_SELECTOR, ".fa-shopping-cart")
    assert len(elements) > 0
    # Assert to check that the SHOP button spells effectively "SHOP"
    assert self.driver.find_element(By.CSS_SELECTOR, ".site-menu > li:nth-child(2) > a").text == "SHOP"
    # Now we start the shopping
    self.driver.find_element(By.CSS_SELECTOR, ".site-menu > li:nth-child(2) > a").click()
    self.driver.find_element(By.CSS_SELECTOR, ".block-4-image .img-fluid").click()
    self.driver.find_element(By.ID, "id_quantity").send_keys("1")
    self.driver.find_element(By.ID, "id_quantity").click()
    self.driver.find_element(By.ID, "id_colour").click()
    dropdown = self.driver.find_element(By.ID, "id_colour")
    dropdown.find_element(By.XPATH, "//option[. = 'green']").click()
    self.driver.find_element(By.ID, "id_colour").click()
    self.driver.find_element(By.ID, "id_size").click()
    dropdown = self.driver.find_element(By.ID, "id_size")
    dropdown.find_element(By.XPATH, "//option[. = 's']").click()
    self.driver.find_element(By.ID, "id_size").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    # Screenshot saving of the current page
    self.driver.save_screenshot("test-fonctionnels-results/screenshot.png")
  
    # Calling test_is_item_size_s() fonction
    self.test_is_item_size_s()
    # Calling test_is_item_colour_green() fonction
    self.test_is_item_colour_green()
    # Calling item_quantity() fonction
    self.item_quantity()

  # Test to check that in the shopping cart there is a T-Shirt Size S
  def test_is_item_size_s(self):
    driver = self.driver
    assert self.driver.find_element(By.CSS_SELECTOR, "small:nth-child(2)").text == "Size: s"

  # Test to check that in the shopping cart there is a T-Shirt Colour green
  def test_is_item_colour_green(self):
    driver = self.driver
    assert self.driver.find_element(By.CSS_SELECTOR, "small:nth-child(3)").text == "Colour: green"
  
  # Test to check that in the shopping cart there is only 1 T-Shirt
  def item_quantity (self):
    driver = self.driver
    value = self.driver.find_element(By.CSS_SELECTOR, ".form-control").get_attribute("value")
    assert value == "1"

  def teardown_method(self, method):
    self.driver.quit()
